import React, {useState} from 'react';
import './App.css';

function App() {

  // const [getter, setter] = useState(initialState)
  const [total, setTotal] = useState(0);
  const [num1, setNum1] = useState(0);
  const [num2, setNum2] = useState(0);

  function Add() {
    setTotal(num1 + num2)
  }

  return (
    <div className="App">
      <div className="p-4">
          <h2>Calculator</h2>

          <div className="py-5" id="total">{total}</div>

          <div className="d-flex mb-5 justify-content-center">
            <input
            className="form-control w-25"
            type="number"
            value={num1}
            onChange={e => setNum1(parseInt(e.target.value))}
            />

            <input
            className="form-control w-25"
            type="number"
            value={num2}
            onChange={e => setNum2(parseInt(e.target.value))}
            />
          </div>
        
        <button className="btn btn-primary mx-1" onClick={Add}>Add</button>
        <button className="btn btn-primary mx-1">Subtract</button>
        <button className="btn btn-primary mx-1">Multiply</button>
        <button className="btn btn-primary mx-1">Divide</button>
        <button className="btn btn-primary mx-1">Reset</button>
      </div>
    </div>
  );
}

export default App;
